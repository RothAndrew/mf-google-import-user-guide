# v0.0.4.0
* Add some more error logging to try to nail down the issue with MF Google Import sometimes being unable to retrieve the contents of the Classes value list.

# v0.0.5.0
* Added more descriptive error messages
* Clarified wording in config tool
* Added ability to send user an email if an import fails
* Fixed gmail search to actually find all emails instead of just the first page (oops!)

# v0.0.7.0
* bug fixes

# v0.0.8.0
* If "Class is first secondary label" is chosen, and the primary label is assigned, the message will be imported with the default class

# v0.0.9.0
* Imports emails with default class if email is in primary label and "class is first secondary label" is chosen

# v0.0.10.0
* label matches are case insensitive

# v0.0.11.0
* Add debug mode with lots more logging.

# v0.0.12.0
* Add support for non alpha-numeric characters in labels

# v0.0.13.0
* If a datetime from the message header "Date" is improperly formatted (meaning, not RFC2822 formatted), it can't be used when importing . Changed the message in the event viewer from an error to a warning. Also added sending an email to the user if that function is enabled in the config tool.

# v0.0.14.0
* Added ability to import auth tokens and added a program to generate and send tokens

# v0.0.15.0
* Added more logging for importing messages

# v0.0.16.0
* take out an annoying info message when debug is on.

# v0.0.17.0
* oops, needed label.name instead of label

# v0.0.18.0
* Minor performance increase

# v0.1.0.0
* Added the ability to import messages in the same thread/conversation as files in a single Multi-File Document in M-Files. If this feature is selected the metadata of the Multi-File Document will reflect the data from the most recent email. The files in the Multi-File document will be named with the template "MessageID - Subject". DO NOT REMOVE THE MESSAGEID FROM THE BEGINNING OF THE FILENAME. THAT'S HOW THE IMPORT TOOL DETECTS IF THAT MESSAGE IS PRESENT ALREADY.
  * When importing messages in a thread/conversation, the tool searches M-Files for documents where the value of the Thread ID property matches the Thread ID of the messages. It will use the first Multi-File document it finds that matches. If no Multi-File document is found it will use the first Single-File document it finds. In order to reduce clutter in your M-Files vault if your vault contains multiple documents with the same ThreadID because you are upgrading from v0.0 or changing how you import documents we advise that you delete all other documents in the system with the same Thread ID after you have verified that the import to the Multi-File document has worked successfully.
* Changed label behavior on successfully imported messages to always add the label "Imported to M-Files"
  * The tool now excludes messages containing the label "Stop Importing to M-Files" when searching. It used to exclude messages containing the label "Imported to M-Files". This was done to clarify how the tool searches for files to import and how to stop the import process for a particular message or thread/conversation if you are using the main import label as a permanent archive of emails.
  * NOTE: Due to these changes if you are upgrading from v0.0 any messages that have already been imported to M-Files but don't have the label "Stop Importing to M-Files" will be included in the list for the tool to import. They will not be imported again because the tool runs a search in M-Files for documents that match the message's Message ID but the import process will run slower since it has more messages to check than is necessary. To fix this issue you can add the "Stop Importing to M-Files" label to messages that have the "Imported to M-Files" label. You can do this on a case by case basis or all at once. Contact support if you need help. 

# v0.1.1.0
* Changed slightly how MFGI matches label entries with value lists to hopefully overcome an issue with "value not found" when it really is there.
* Added a listing of the value list contents to the "value not found" error message.

# v0.1.2.0
* BREAKING CHANGE: Changed the tool to ask for Class ID instead of Class Name. This is affected in the "Fixed Class to Use" textbox in the configuration tool, as well as the value of the "first secondary label as class". This was done to correct an intermittent but persistent error in users' vaults that does not allow the tool to retrieve the list of classes. When this issue is fixed the change will be reverted back to the Class name while still allowing for class ID.

# v1.0.0.0
* Finally out of beta!
* Added arbitration and lawyers fees clauses to EULA.

# v1.0.1.0
* Made adding the "Imported to M-Files" label optional. It is controlled via a checkbox in the configuration tool.

# v1.0.2.0
* Added MF Google Import version number to most error messages and emails.
* Checks for existence of Google authentication token before trying to use it.
* Only checks out a document if it does not already contain all of the messages in a conversation.

# v1.1.0.0
* Added the ability to choose whether attachments are imported or ignored
* Added metadata properties for Number of Attachments, Attachment File Names, and Message Size (including attachments).

# v1.2.0.0
* Bugfix: Already imported emails will get the "Stop Importing to M-Files" label if the user switches "Import New Messages In a Conversation" to "No" later on.